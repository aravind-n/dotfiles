[[ $- != *i* ]] && return

# prompt settings

if [[ $UID == 0 ]]; then
    export PS1="\[\e[0;31m\]\u\[\e[0m\]@\h:\W # "
else
    export PS1="\[\e[0;32m\]\u\[\e[0m\]@\h:\W $ "
fi

# enable colored ls output
unamestr=$(uname)
if [ "$unamestr" = "Darwin" ]; then
    alias ls='ls -G'
elif [ "$unamestr" = "Linux" ]; then
    alias ls='ls --color=auto'
fi
unset unamestr

# pyenv
eval "$(pyenv init --path)"

# Rust setup
. "$HOME/.cargo/env"
