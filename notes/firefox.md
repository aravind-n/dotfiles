Firefox
=======

Add-ons I use for Firefox:
* [Cookie
  AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)
* [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)
* [Facebook
  Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/)
* [HTTPS Everywhere](https://www.eff.org/https-everywhere)
* [Privacy Badger](https://www.eff.org/pages/privacy-badger)
* [Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
* [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
