Editors
=======

My primary text editor for most small tasks is vim. For other tasks, I use
VSCode. I'm currently looking into switching to VSCodium which is a FOSS build
of VSCode

Plugins I always use on VSCode are:
* vscodevim

