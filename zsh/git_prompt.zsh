#!/usr/local/bin/zsh

# Forked from https://gist.github.com/1712320

setopt prompt_subst
autoload -U colors && colors # Enable colors in prompt

GIT_STATE_MERGE="Merging"
GIT_STATE_REVERT="Reverting"
GIT_STATE_CHERRY_PICK="Cherry Picking"
GIT_STATE_BISECT="Bisecting"
GIT_STATE_REBASE="Rebasing"
GIT_STATE_AM="AM"
GIT_STATE_AM_REBASE="AM/Rebase"
GIT_STATUS_STASHED="\$"
GIT_STATUS_MODIFIED="!"
GIT_STATUS_STAGED="+"
GIT_STATUS_UNTRACKED="?"
GIT_STATUS_AHEAD="ANUM"
GIT_STATUS_BEHIND="BNUM"

# Show Git branch/tag, or name-rev if on detached head
parse_git_branch() {
    (git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD) 2> /dev/null
}

# Show different symbols as appropriate for various Git repository statuses
parse_git_status() {
    # Compose this value via multiple conditional appends.
    local GIT_STATUS=""

    local NUM_AHEAD="$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')"
    if [ "$NUM_AHEAD" -gt 0 ]; then
        GIT_STATUS=$GIT_STATUS${GIT_STATUS_AHEAD//NUM/$NUM_AHEAD}
    fi

    local NUM_BEHIND="$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')"
    if [ "$NUM_BEHIND" -gt 0 ]; then
        GIT_STATUS=$GIT_STATUS${GIT_STATUS_BEHIND//NUM/$NUM_BEHIND}
    fi

    local NUM_STASH="$(git stash list 2> /dev/null | wc -l | tr -d ' ')"
    if [ "$NUM_STASH" -gt 0 ]; then
        GIT_STATUS=$GIT_STATUS$GIT_STATUS_STASHED
    fi

    if ! git diff --quiet 2> /dev/null; then
        GIT_STATUS=$GIT_STATUS$GIT_STATUS_MODIFIED
    fi

    if ! git diff --cached --quiet 2> /dev/null; then
        GIT_STATUS=$GIT_STATUS$GIT_STATUS_STAGED
    fi

    local ROOT="$(git rev-parse --show-toplevel 2> /dev/null)"
    if [[ -n $(git ls-files $ROOT --other --exclude-standard 2> /dev/null) ]]; then
        GIT_STATUS=$GIT_STATUS$GIT_STATUS_UNTRACKED
    fi

    if [[ -n $GIT_STATUS ]]; then
        GIT_STATUS="%{$fg_bold[green]%}[$GIT_STATUS]%{$reset_color%}"
        echo -n "$GIT_STATUS"
    fi
}

parse_git_state() {
    # Compose this value via multiple conditional appends.
    local GIT_STATE=""

    local GIT_DIR="$(git rev-parse --git-dir 2> /dev/null)"
    if [ -n $GIT_DIR ] && test -r $GIT_DIR/MERGE_HEAD; then
        GIT_STATE=$GIT_STATE$GIT_STATE_MERGE
    fi

    if [ -n $GIT_DIR ] && test -r $GIT_DIR/REVERT_HEAD; then
        GIT_STATE=$GIT_STATE$GIT_STATE_REVERT
    fi

    if [ -n $GIT_DIR ] && test -r $GIT_DIR/CHERRY_PICK_HEAD; then
        GIT_STATE=$GIT_STATE$GIT_STATE_CHERRY_PICK
    fi

    if [ -n $GIT_DIR ] && test -r $GIT_DIR/BISECT_LOG; then
        GIT_STATE=$GIT_STATE$GIT_STATE_BISECT
    fi

    if [ -n $GIT_DIR ] && test -r $GIT_DIR/rebase-merge; then
        GIT_STATE=$GIT_STATE$GIT_STATE_REBASE
    elif test -d $GIT_DIR/rebase-apply; then
        if test -f $GIT_DIR/rebase-apply/rebasing; then
            GIT_STATE=$GIT_STATE$GIT_STATE_REBASE
        elif test -f $GIT_DIR/rebase-apply/applying; then
            GIT_STATE=$GIT_STATE$GIT_STATE_AM
        else
            GIT_STATE=$GIT_STATE$GIT_STATE_AM_REBASE
        fi
    fi

    if [[ -n $GIT_STATE ]]; then
        GIT_STATE="%{$fg_bold[yellow]%}[$GIT_STATE]%{$reset_color%}"
        echo -n $GIT_STATE
    fi
}

# If inside a Git repository, print its branch and state
git_prompt_string() {
    local git_where="$(parse_git_branch)"
    [ -n "$git_where" ] && echo -n "$(parse_git_state)$(parse_git_status)%{$fg[green]%}[${git_where#(refs/heads/|tags/)}]%{$reset_color%}"
}

# Set the right-hand prompt
RPS1='$(git_prompt_string)'
