# Fish shell config
# Author: aravind

if status is-interactive
    # Disable greeting message
    set fish_greeting

    # Starship
    starship init fish | source
end

# OS specific settings
set -l unamestr (uname)
if test "$unamestr" = "Darwin"
    alias sshfs="sshfs -o noappledouble,follow_symlinks"
end
set --erase unamestr

# Transmission aliases
alias tsmd="transmission-daemon"
alias tsmr="transmission-remote"
