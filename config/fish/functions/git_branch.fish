# Get current git branch
# Author: aravind

function git_branch
    set -l branch (git symbolic-ref --short -q HEAD 2> /dev/null; or git name-rev --name-only --no-undefined --always HEAD 2> /dev/null)
    if test -n "$branch"
        printf "%s" (set_color green)"[$branch]"(set_color normal)
    end
end
