# Get git status message
# Author: aravind

function git_status
    set -l conflicted (_git_diff_status -c)
    set -l stashed (_git_stashed_status)
    set -l deleted (_git_diff_status -d)
    set -l renamed (_git_diff_status -r)
    set -l modified (_git_diff_status -m)
    set -l typechanged (_git_diff_status -t)
    set -l staged (_git_staged_status)
    set -l untracked (_git_untracked_status)
    set -l ahead (_git_commits_ahead)
    set -l behind (_git_commits_behind)
    set -l git_status "$conflicted$stashed$deleted$renamed$modified$typechanged$staged$untracked$ahead$behind"

    if test -n "$git_status"
        printf "%s" (set_color brgreen)"[$git_status]"(set_color normal)
    end
end

function _git_diff_status
    set -f symbol ""
    set -f filter ""
    argparse --name=_git_diff_status c/conflicted d/deleted r/renamed m/modified t/typechanged -- $argv
    or return
    if set -q _flag_conflicted
        set -f symbol "="
        set -f filter U
    else if set -q _flag_deleted
        set -f symbol x
        set -f filter D
    else if set -q _flag_renamed
        set -f symbol "»"
        set -f filter R
    else if set -q _flag_modified
        set -f symbol "!"
        set -f filter M
    else if set -q _flag_typechanged
        set -f symbol ""
        set -f filter T
    else
        return
    end

    set -l diff_result (git diff --name-only --diff-filter=$filter)
    if test -n "$diff_result"
        printf "%s" "$symbol"
    end
end

function _git_stashed_status
    set -l symbol "\$"
    set -l number_of_stashes (git stash list 2> /dev/null | wc -l | tr -d ' ')
    if test $number_of_stashes -gt 0
        printf "%s" "$symbol"
    end
end

function _git_staged_status
    set -l symbol '+'
    if not git diff --cached --quiet 2>/dev/null
        printf "%s" $symbol
    end
end

function _git_untracked_status
    set -l symbol '?'
    set -l root (git rev-parse --show-toplevel)
    set -l diff (git ls-files $root --other --exclude-standard 2> /dev/null)
    if test -n "$diff"
        printf "%s" $symbol
    end
end

function _git_commits_ahead
    set -l symbol A
    set -l commits_ahead (git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')
    if test $commits_ahead -gt 0
        printf "%s" "$symbol$commits_ahead"
    end
end

function _git_commits_behind
    set -l symbol B
    set -l commits_behind (git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')
    if test $commits_behind -gt 0
        printf "%s" "$symbol$commits_behind"
    end
end
