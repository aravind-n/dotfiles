# Git state message
# Author: aravind

function git_state
    # TODO get the progress of each step
    set -l rebase_am (_git_rebase_am_state)
    set -l merge (_git_merge_state)
    set -l revert (_git_revert_state)
    set -l cherry_pick (_git_cherry_pick_state)
    set -l bisect (_git_bisect_state)
    set -l git_state "$rebase_am$merge$revert$cherry_pick$bisect"

    printf "%s" (set_color --bold yellow)"$git_state"(set_color normal)
end

function _git_rebase_am_state
    set git_dir (git rev-parse --git-dir 2> /dev/null)
    if test -d $git_dir/rebase-merge
        printf "%s" "[Rebasing]"
    else if test -d $git_dir/rebase-apply
        if test -f $git_dir/rebase-apply/rebasing
            printf "%s" "[Rebasing]"
        else if -f $git_dir/rebase-apply/applying
            printf "%s" "[AM]"
        else
            printf "%s" "[AM/Rebase]"
        end
    end
end

function _git_merge_state
    set git_dir (git rev-parse --git-dir 2> /dev/null)
    if test -r $git_dir/MERGE_HEAD
        printf "%s" "[Merging]"
    end
end

function _git_revert_state
    set git_dir (git rev-parse --git-dir 2> /dev/null)
    if test -r "$git_dir/REVERT_HEAD"
        printf "%s" "[Reverting]"
    end
end

function _git_cherry_pick_state
    set git_dir (git rev-parse --git-dir 2> /dev/null)
    if test -r "$git_dir/CHERRY_PICK_HEAD"
        printf "%s" "[Cherry picking]"
    end
end

function _git_bisect_state
    set git_dir (git rev-parse --git-dir 2> /dev/null)
    if test -f "$git_dir/BISECT_LOG"
        printf "%s" "[Bisecting]"
    end
end
